# -----------> ACTIVITY SOLUTION <-----------
# ---> INSTRUCTIONS <---
# 1.Create a car dictionary with the following keys: brand, model, year_of_make, color (any values).
# 2. Print the following statement from the details: "I own a (color) (brand) (model) and it was made in (year_of_make)."
# 3. Create a function that gets the square of a number 
# 4. Create a function that takes one of the following languages as its parameter:
# --> a. French
# --> b. Spanish
# --> c. Japanese
# Depending on which language is given, the function prints "Hello World!" in that language. Add a condition that asks the user to input a valid language if the given paramater does not match any of the above.

# Part 1 and 2
car = {
    "brand" : "ford",
    "model" : "territory",
    "year_of_make" : 2022,
    "color" : "white"
}
print(f'I own a {car["color"]} {car["brand"]} {car["model"]} and it was made in {car["year_of_make"]}.')

# Part 3
def square(num):
    return num**2

result = (square(int(input('Input any number: '))))
print(result)


# Part 4
def greeting(language) : 
    if language == "French" or language == "french" :
        print("Bonjour le monde!")
    elif language == "Spanish" or language == "spanish" :
        print("Hola Mundo!")
    elif language == "Japanese" or language == "japanese" :
        print("こんにちは世界")
    else :
        print("Input a valid language.")

greeting(input('Enter a language: '))
